﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace recuperacao
{
    /// <summary>
    /// Lógica interna para FrmConsulFunc.xaml
    /// </summary>
    public partial class FrmConsulFunc : Window
    {
        public FrmConsulFunc()
        {
            InitializeComponent();
            CmbBuscarCliente.Focus();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            FrmMenu frm = new FrmMenu();
            frm.Show();
            this.Close();
        }

        private void FrmConsulFunc1_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idFuncionario, nomeFunc FROM funcionario";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbBuscarCliente.DisplayMemberPath = "nomeFunc";
            CmbBuscarCliente.ItemsSource = dt.DefaultView;
        }

        private void CmbBuscarCliente_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            string selecionar = "SELECT * FROM funcionario WHERE nomeFunc = ?";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);
            com.Parameters.Clear();
            com.CommandType = CommandType.Text;

            MySqlDataReader dr = com.ExecuteReader();
            dr.Read();
            if (CmbBuscarCliente.Text != "")
            {
                TxtCodigo.Text = dr.GetString(0);
                TxtNome.Text = dr.GetString(1);
                TxtEmail.Text = dr.GetString(10);
                TxtSenha.Text = dr.GetString(11);
                TxtTelefone.Text = dr.GetString(9);
                TxtDataCadastro.Text = dr.GetString(13);
            }
        }

        private void BtnAlterar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            string alterar = "UPDATE funcionario SET " +
                "nomeFunc= '" + TxtNome.Text + "'," +
                "email='" + TxtEmail.Text + "'," +
                "dataadmissao='" + TxtDataCadastro.Text + "'," +
                "WHERE idFuncionario = '" + TxtCodigo.Text + "'";
        }

        private void Limpar()
        {
            TxtCodigo.Clear();
            TxtNome.Clear();
            TxtEmail.Clear();
            TxtSenha.Clear();
            TxtDataCadastro.Clear();

        }

        private void BtnDeletar_Click(object sender, RoutedEventArgs e)
        {
            var del = MessageBox.Show(
               "Tem certeza que deseja DELETAR esse funcionário?",
               "Deletar Funcionário",
               MessageBoxButton.YesNo,
               MessageBoxImage.Exclamation);

            if (del != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                Banco bd = new Banco();
                bd.Conectar();
                string deletar = "DELETE FROM funcionario WHERE idFuncionario'" + TxtCodigo.Text + "'";
                MySqlCommand comandos = new MySqlCommand(deletar, bd.conexao);
                comandos.ExecuteNonQuery();
                MessageBox.Show("Funcionário excluído com sucesso!");
                bd.Desconectar();
                Limpar();
                // Carrega novamente a comboboxCliente
                Banco bd1 = new Banco();
                bd1.Conectar();
                string selecionar = "SELECT idFuncionario, nomeFunc FROM funcionario";
                MySqlCommand com = new MySqlCommand(selecionar, bd1.conexao);
                MySqlDataAdapter da = new MySqlDataAdapter(com);
                DataTable dt = new DataTable();
                da.Fill(dt);
                CmbBuscarCliente.DisplayMemberPath = "nomeFunc";
                CmbBuscarCliente.ItemsSource = dt.DefaultView;
            }
        }
    }
}