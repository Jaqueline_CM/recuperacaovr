﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace recuperacao
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        int i = 0;
        string logado;

        public MainWindow()
        {
            InitializeComponent();
            TxtFuncionario.Focus();
        }

        private void TxtFuncionario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                BtnLogin.IsEnabled = true;
                BtnLogin.Focus();
            }
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM funcionario WHERE email = @funcionario AND senha = @senha", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@funcionario", MySqlDbType.String).Value = TxtFuncionario.Text;
            comm.Parameters.Add("@senha", MySqlDbType.String).Value = TxtSenha.Password;

            //CASO EXISTA O USUÁRIO
            try
            {
                comm.CommandType = System.Data.CommandType.Text; /*Executa o comando*/
                                                                 //recebe o conteúdo do banco
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                acesso.nivelAcesso = dr.GetString(5);

                BtnLogin.IsEnabled = false;
                Thread.Sleep(20);//pausa de 10 milésimos de seg
                PgbLogin.Value = 0;
                i = 0;
                Task.Run(() =>
                {
                    while (i < 100)
                    {
                        i++;
                        Thread.Sleep(50);
                        this.Dispatcher.Invoke(() => //Usar para atualização imediata (obrigatório)
                        {
                            PgbLogin.Value = i;
                            TxtCarrega.Text = i + "%";
                            while (i == 100)
                            {
                                logado = acesso.nivelAcesso;
                                if (acesso.nivelAcesso == "GERENTE")
                                {
                                    Hide();
                                    FrmMenu frm = new FrmMenu();
                                    frm.Show();
                                    frm.lblBemVindo.Content = logado;
                                    break;
                                }
                                else if (acesso.nivelAcesso == "CAIXA")
                                {
                                    Hide();
                                    FrmMenu frm = new FrmMenu();
                                    frm.Show();
                                    frm.lblBemVindo.Content = logado;
                                    frm.BtnCadFuncionario.IsEnabled = false;
                                    break;
                                }
                                else if (acesso.nivelAcesso == "PADEIRO")
                                {
                                    Hide();
                                    FrmMenu frm = new FrmMenu();
                                    frm.Show();
                                    frm.lblBemVindo.Content = logado;
                                    frm.BtnConFuncionario.IsEnabled = false;
                                    frm.BtnCadFuncionario.IsEnabled = false;
                                    frm.BtnConProduto.IsEnabled = false;
                                    frm.BtnCadProduto.IsEnabled = false;
                                    break;
                                }
                            }
                        });
                    }
                });
            }
            //CASO NÃO EXISTA O USUÁRIO
            catch
            {
                MessageBox.Show("Favor preencher o usuario e senha corretamente!!!");
                TxtFuncionario.Clear();
                TxtSenha.Clear();
                TxtFuncionario.Focus();
                PgbLogin.Value = 0;
                TxtSenha.IsEnabled = false;
                BtnLogin.IsEnabled = false;
                TxtCarrega.Text = "0%";
            }
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show("Tem certeza que deseja ENCERRAR o sistema?", "Encerrar sistema",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);

            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown(); //Encerrar a aplicação que está em execução!
            }
        }
    }
}
