﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace recuperacao
{
    /// <summary>
    /// Lógica interna para FrmMenu.xaml
    /// </summary>
    public partial class FrmMenu : Window
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void BtnConFuncionario_Click(object sender, RoutedEventArgs e)
        {
            FrmConsulFunc frm = new FrmConsulFunc();
            frm.Show();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            MainWindow frm = new MainWindow();
            frm.Show();

        }

        private void BtnCadFuncionario_Click(object sender, RoutedEventArgs e)
        {
            FrmCadFunc frm = new FrmCadFunc();
            frm.Show();
        }
    }
}
