﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VilaReal
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {

        int i = 0;
        string funcionario, senha;

        public MainWindow()
        {
            InitializeComponent();
            TxtFuncionario.Focus();

        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                senha = TxtSenha.Password;
                BtnEntrar.IsEnabled = true;
                BtnEntrar.Focus();
            }
        }

        private void BtnEntrar_Click(object sender, RoutedEventArgs e)
        {
            BtnEntrar.IsEnabled = false;

            Thread.Sleep(10); //Pausa de 10 milessimos segundos
            PgbLogin.Value = 0;
            i = 0;
            Task.Run(() =>
            {
                while (i < 100)
                {
                    i++;
                    Thread.Sleep(50);
                    this.Dispatcher.Invoke(() => //usar para atualização imediata (obrigatorio)
                    {
                        PgbLogin.Value = i;
                        TxtCarrega.Text = i + "%";
                        while (i == 100)
                        {
                            if (funcionario == "ADMIN" && senha == "1234")
                            {
                                Hide();
                                FrmMenu frm = new FrmMenu();
                                frm.Show();
                                frm.lblLogado.Content = funcionario;
                                break;
                            }
                            else if (funcionario == "TESTE" && senha == "5678")
                            {
                                Hide();
                                FrmMenu frm = new FrmMenu();
                                frm.Show();
                                frm.lblLogado.Content = funcionario;
                                frm.btnCadastro.IsEnabled = false;
                                frm.btnRelatorio.IsEnabled = false;
                                break;
                            }
                            else
                            {
                                MessageBox.Show("Favor preencher o usuario ou a senha corretamente!!!");
                                TxtFuncionario.Clear();
                                TxtSenha.Clear();
                                TxtFuncionario.Focus();
                                PgbLogin.Value = 0;
                                TxtSenha.IsEnabled = false;
                                TxtCarrega.Text = "0%";
                            }
                            break;
                        }
                    });
                }
            });
        }

        private void TxtFuncionario_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Enter)
            {
                funcionario = TxtFuncionario.Text;
                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


    }
}
