﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VilaReal
{
    /// <summary>
    /// Lógica interna para FrmCadFuncionario.xaml
    /// </summary>
    public partial class FrmCadFuncionario : Window
    {
        public FrmCadFuncionario()
        {
            InitializeComponent();
            TxtNome.Focus();
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime dataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string inserir = "INSERT INTO funcionario(nomeFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,idEmpresa)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    TxtSenha.Text + "','" +
                   CmbNivel.Text + "','" +
                   status + "','" +
                   dataCadastro.ToString("yyy-MM-dd") + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }

            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO funcionario(nomeFunc,emailFunc,senhaFunc,nivelFunc,statusFunc,dataCadFunc,idEmpresa)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    TxtSenha.Text + "','" +
                   CmbNivel.Text + "','" +
                   status + "','" +
                   dataCadastro.ToString("yyy-MM-dd") + "','" +
                   codigoEmp + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Funcionário cadastrado com sucesso!!!", "Cadastro de Funcionário");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            var tel = MessageBox.Show(
                "Deseja inserir um número de telefone para esse funcionário?",
                "Cadastro de Telefone", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

            if (tel != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                FrmCadTelFuncionario frm = new FrmCadTelFuncionario();
                frm.Show();
                this.Close();
            }
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();
            }
        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtDataCadastro.Text = DateTime.Now.ToShortDateString();

                CmbNivel.IsEnabled = true;
                CmbNivel.Focus();


            }
        }

        private void CmbNivel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChkStatus.IsEnabled = true;
            MessageBox.Show("Não esqueça do Status do Funcionário");
            CmbEmpresa.IsEnabled = true;
            CmbEmpresa.Focus();
        }

        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Banco bd = new Banco();
                bd.Conectar();
                MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nomeEmp = ?", bd.conexao);
                comm.Parameters.Clear();
                comm.Parameters.Add("@nomeEmp", MySqlDbType.String).Value = CmbEmpresa.Text;

                comm.CommandType = CommandType.Text; /*Executa o comando*/
                                                     // recebe o conteúdo do banco
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();

                codigoEmp = dr.GetString(0);


                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();

            }
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.IsEnabled = true;
            TxtNome.Focus();
            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.Clear();
            TxtEmail.Clear();
            TxtSenha.Clear();
            TxtDataCadastro.Clear();
            ChkStatus.IsChecked = false;


            TxtNome.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            TxtSenha.IsEnabled = false;
            ChkStatus.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
        }

        private void FrmCadFuncionario_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nomeEmp FROM empresa";

            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nomeEmp";
            CmbEmpresa.ItemsSource = dt.DefaultView;
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            FrmMenuCadastro frm = new FrmMenuCadastro();
            frm.Show();
            this.Close();
        }
    }
}
