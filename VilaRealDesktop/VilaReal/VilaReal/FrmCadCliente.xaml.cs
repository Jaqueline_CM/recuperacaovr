﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VilaReal
{
    /// <summary>
    /// Lógica interna para FrmCadCliente.xaml
    /// </summary>
    public partial class FrmCadCliente : Window
    {
        string arquivoSelecionado;
        public FrmCadCliente()

        {
            InitializeComponent();
            TxtNome.Focus();
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;
            DateTime dataCadastro = DateTime.Today;

            Banco bd = new Banco();
            bd.Conectar();
            if (ChkStatus.IsChecked == true)
            {
                status = "ATIVO";
                string inserir = "INSERT INTO cliente(nomeCli,emailCli,senhaCli,statusCli,dataCadCli,fotoCli)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    status + "','" +
                    dataCadastro.ToString("yyyy-MM-dd") + "')";
                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else
            {
                status = "INATIVO";
                string inserir = "INSERT INTO cliente(nomeCli,emailCli,senhaCli,statusCli,dataCadCli,fotoCli)VALUES('" +
                    TxtNome.Text + "','" +
                    TxtEmail.Text + "','" +
                    status + "','" +
                    dataCadastro.ToString("yyyy-MM-dd") + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            bd.Desconectar();
            MessageBox.Show("Cliente cadastrado com sucesso!!!", "Cadastro de Clientes");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            var tel = MessageBox.Show(
                "Deseja inderir um numero de telefone para esse cliente?",
                "Cadastro Telefone",
                MessageBoxButton.YesNo,
                MessageBoxImage.Exclamation);

            if (tel != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                FrmCadTelCliente frm = new FrmCadTelCliente();
                frm.Show();

                this.Close();
            }
        }

        private void TxtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)

            {
                TxtEmail.IsEnabled = true;
                TxtEmail.Focus();
            }
        }

        private void TxtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxtSenha.IsEnabled = true;
                TxtSenha.Focus();

            }
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.Clear();
            TxtEmail.Clear();
            TxtDataCadastro.Clear();
           

            TxtNome.IsEnabled = false;
            TxtEmail.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnSalvar.IsEnabled = false;
            BtnNovo.IsEnabled = false;
            BtnNovo.Focus();
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            TxtNome.IsEnabled = true;
            TxtNome.Focus();
            BtnLimpar.IsEnabled = true;
            BtnNovo.IsEnabled = false;
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            frmMenuCadastro frm = new frmMenuCadastro();
            frm.Show();

            this.Close();

        }

        private void TxtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            {
                if (e.Key == Key.Enter)
                {
                    TxtDataCadastro.Text = DateTime.Now.ToShortDateString();
                    
                    MessageBox.Show("Não esqueça do status do Cliente");
                    BtnSalvar.IsEnabled = true;
                    
                }
            }
        }
    }
}
